# CHANGELOG

## v1.13.2 (2024-10-01)

### Fix

* fix: input file section ordering and formatting ([`94c28af`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/94c28af57e51434dd02b0e2d350e0108198018b1))

* fix: move atomic radii spec after basis ([`7a8e1ee`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/7a8e1eec7b1f4f161b25b8f0eb2a5085c78f17b3))

* fix: add gen keyword to route section if no ecp present ([`9fdbae1`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/9fdbae17bd4a775b9c79dde0ca54394cd55a56b8))

### Unknown

* Merge branch &#39;70-wrong-basis-set-used-if-no-ecp-present&#39; into &#39;main&#39;

Resolve &#34;Wrong basis set used if no ECP present&#34;

Closes #70

See merge request chilton-group/gaussian_suite!77 ([`70f21c7`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/70f21c776a290541c047d84e785333d3f2b15e61))

## v1.13.1 (2024-07-22)

### Fix

* fix: correct number of modes in distort sanity check ([`5c5f8ca`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/5c5f8ca990016f463dfc1ca09fffd9bf02ba1004))

### Unknown

* Merge branch &#39;69-definition-of-n_modes-in-the-distort_func&#39; into &#39;main&#39;

Resolve &#34;Definition of n_modes in the distort_func&#34;

Closes #69

See merge request chilton-group/gaussian_suite!76 ([`55083e9`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/55083e981024bc310ceaeddcf9f6b34f51293c49))

## v1.13.0 (2024-05-23)

### Ci

* ci: add force docs option and docs badge ([`c119db5`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/c119db52ffdfc0828adc7fdde50c0b07ec1b7a12))

* ci: add force docs option and docs badge ([`d4677f5`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/d4677f5e501d1bbe02e9cf5ba3956b5fd47f03ed))

### Feature

* feat: gadi support ([`2b5b0fc`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/2b5b0fcd0b475e0817042d495f44ab7a837a449f))

* feat: gadi support ([`6c13c30`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/6c13c30a66d66192439fc57379106b6a7797e982))

### Fix

* fix: read nmodes explicitly

Explicitly read number of modes for reshape of normal mode vectors
instead of relying on 3N x 3N-6 shape. ([`195a35e`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/195a35eb659b6f2506e496d47ab8727c18353a40))

### Unknown

* CI:remove repeated variable definition from pyproject.toml [skip ci] ([`943af24`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/943af24c907df951708b30e6cbbbe854e253b7ce))

* Merge branch &#39;68-fix-fchk-mode-extractor-with-frozen-atoms&#39; into &#39;main&#39;

Resolve &#34;fix fchk mode extractor with frozen atoms&#34;

Closes #68

See merge request chilton-group/gaussian_suite!75 ([`f5c8a86`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/f5c8a8619b48555a6d48e5a513e91de6049f57fa))

* Merge branch &#39;67-gadi-support&#39; into &#39;main&#39;

Resolve &#34;Gadi support&#34;

Closes #67

See merge request chilton-group/gaussian_suite!74 ([`26c5df6`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/26c5df664d1835ef372d1afb39e83c959108f496))

* Update pyproject.toml [skip ci] ([`3cb3885`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/3cb388524cf30d2283a081caefd1f9ed024acb21))

* Update pyproject.toml ([`ece5ef1`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/ece5ef17def5cc6b5e90a422423294b31c55c0d9))

* Update .gitlab-ci.yml file ([`f1a4b77`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/f1a4b778b68146707f0f28a345b10d8d35b03a00))

* Update .gitlab-ci.yml file for python semantic release v8 ([`f7fe860`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/f7fe860e44cc47682908f191eb292e89baf91c2d))

* Update .gitlab-ci.yml file ([`d3177be`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/d3177bec822d197e7ffe078819b913753a769eb5))

* Update .gitlab-ci.yml file ([`7193a54`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/7193a546ff628b8af0c306f0e122a0f8e6140781))

## v1.12.1 (2023-09-07)

### Fix

* fix: generalise irrep label pattern

Irrep label pattern now captures letters, numbers and apostrophes. ([`24c1b9d`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/24c1b9df1412e021107b9a94dfe87eb7aec7718c))

### Unknown

* Update .gitlab-ci.yml file ([`698f23f`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/698f23ff992f5fa375c6fef413599cfbbe6f51db))

* Merge branch &#39;66-refine-freq-extractor-regex&#39; into &#39;main&#39;

Resolve &#34;refine freq extractor regex&#34;

Closes #66

See merge request chilton-group/gaussian_suite!73 ([`949aa76`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/949aa76fa825f748b03cdf5af19d85bc431994ae))

## v1.12.0 (2023-06-21)

### Unknown

* Merge branch &#39;64-z_matrix-input-error&#39; into &#39;main&#39;

Resolve &#34;Z_matrix input error&#34;

Closes #64

See merge request chilton-group/gaussian_suite!71 ([`0f9bf3d`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/0f9bf3d0ebe3e7e885d754de31011c9f3b9b1360))

* Fix: Correct number of empty lines added ([`871b200`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/871b200e38b44e077f52ae7ec5d2e4de4ffb7299))

## v1.11.3 (2023-06-19)

### Fix

* fix: added squirly braced to print out ([`c11c97f`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/c11c97f8e5d447b23532a971c35282177fb91cdc))

### Unknown

* Merge branch &#39;63-z_matrix-error&#39; into &#39;main&#39;

Resolve &#34;z_matrix error&#34;

Closes #63

See merge request chilton-group/gaussian_suite!70 ([`73f1b55`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/73f1b55ed94532b7a7bf8fe93e336194b4cdce75))

## v1.11.2 (2023-06-19)

### Fix

* fix: added conversion to z-matrix for .com files ([`a304dec`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/a304decb4c24f04a9f2326d8a441af83c0cd706f))

### Unknown

* Merge branch &#39;61-z-matrix-conversion&#39; into &#39;main&#39;

Resolve &#34;z-matrix conversion&#34;

Closes #61

See merge request chilton-group/gaussian_suite!68 ([`44edc9c`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/44edc9cbd66957fa5637f82409384b8856579c99))

## v1.11.1 (2023-05-31)

### Fix

* fix: single column bug

Read last vibration correctly if it occupies a single column. ([`8f6f96e`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/8f6f96eaae4242979bfa93b74b989c4644da2237))

### Refactor

* refactor: adjust file permissions

Freq extractor now only requires read permissions. ([`c4d8830`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/c4d8830cb8674e23a2f35f2315d707cc6384fee7))

### Unknown

* Merge branch &#39;60-freq-extractor-single-column-bug&#39; into &#39;main&#39;

Resolve &#34;freq extractor single column bug&#34;

Closes #60

See merge request chilton-group/gaussian_suite!67 ([`88f1fc7`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/88f1fc710400a9d06d7df3574860ab1621a86b87))

* Update .gitlab-ci.yml file ([`5763a32`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/5763a32d07fcee35612159eca395d4ac6de7a5f5))

## v1.11.0 (2023-02-21)

### Feature

* feat: atomic number extractor

extracts atomic number from fchk. No front end ([`d23078d`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/d23078deced4bbc3174dcc3fa1eaa5b8068dd6ad))

### Unknown

* Merge branch &#39;58-add-atomic-number-extractor-for-fchk-files&#39; into &#39;main&#39;

Resolve &#34;add atomic number extractor for fchk files&#34;

Closes #58

See merge request chilton-group/gaussian_suite!66 ([`e3989a3`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/e3989a331b09a78c01764e160c6a8c12bb795961))

## v1.10.2 (2023-02-20)

### Fix

* fix: resolve conflict

resolves conflict overlooked after popping the stash ([`3c87bbe`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/3c87bbe099bc0589020e2eb539afb666d7dec593))

### Refactor

* refactor: modernise selection argument

Safety first ([`40979e4`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/40979e4b027ca3dd2a1b5967e08ddea8664b6ed8))

### Unknown

* Merge branch &#39;57-update-selection-arguments&#39; ([`b35a890`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/b35a890969519c7145cf7392d4474effc651510e))

* Merge branch &#39;57-update-selection-arguments&#39; into &#39;main&#39;

Resolve &#34;update selection arguments&#34;

Closes #57

See merge request chilton-group/gaussian_suite!65 ([`b328136`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/b328136016caa2141a4cd5fe15227a847b7e16c2))

## v1.10.1 (2022-09-29)

### Build

* build: update python to latest ([`fe72eaa`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/fe72eaa1f7e5f7af7c493c7eddd2be3a0d6479c8))

### Fix

* fix: add conversion factor from napierian to linear absorbance ([`e32180a`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/e32180a9c252d47889366170c70c6bb1428f1b25))

### Unknown

* Merge branch &#39;main&#39; of https://www.gitlab.com/chilton-group/gaussian_suite into main ([`7b6a9ee`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/7b6a9eec659155fc08352e9682d3a503d66b963c))

## v1.10.0 (2022-09-29)

### Build

* build: bump python vers ([`bfa0df6`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/bfa0df6d30d322848354f74ef94108718c426230))

* build: add phenol log file example ([`82e469a`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/82e469ac2b3bf893258ed1d795f21309821c011b))

* build: add phenol test fchk ([`711bf3f`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/711bf3f6e6e636e8cc80da44904335daccc4f955))

### Feature

* feat: add extractor support for displacements in fchk ([`fe905cc`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/fe905cc176425f89836f8423aa4e82fa1e7074b8))

* feat: Add SaveNormalModes to freq calculation ([`3ad7bf8`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/3ad7bf81111d0548111ae71974d24bd2814ef974))

* feat: allow float or list of fwhm and areas ([`b7d870c`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/b7d870cf27b48b4d246145336eb935c6810603e4))

* feat: add ir intensity plotting and new dipole derivative extraction ([`6ec9942`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/6ec994212902b81da1f56fa6d3b0af5e08d9a1ea))

### Fix

* fix: bug in ir spectrum datafile save option ([`4d61da9`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/4d61da98d123572e469e25cb7eb41c69c88c8457))

* fix: bug in dip_deriv ([`c748c5f`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/c748c5fd2a11f595f049b1f7035b92ce3199a43e))

### Refactor

* refactor: simplifiy infrared intensity calculation code ([`b21f51c`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/b21f51c97cfb8173796a51d9bd631381bf4bde82))

* refactor: update cli functions to use extractor interface ([`63f3785`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/63f3785f18200677f97c352df0673b40123d3e49))

* refactor: remove old freq_extractor code, add new testing files ([`bbef916`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/bbef916e9ae135cbbc7dff56b5bba802dfb7fcac))

### Style

* style: minor changes ([`716e299`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/716e299c697193ed1c9fbbc9e50ea60a3a1011de))

### Unknown

* Merge branch &#39;55-add-infrared-spectrum-plot-option&#39; into &#39;main&#39;

Resolve &#34;Add Infrared Spectrum Plot option&#34;

Closes #55

See merge request chilton-group/gaussian_suite!63 ([`901e3fa`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/901e3faf3ede0bfbae0d82129aee9f8f79cce729))

## v1.9.0 (2022-09-15)

### Build

* build: bump hpc_suite vers number ([`f04a2ab`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/f04a2abcbd32de78669358413373b49a116a3c56))

### Feature

* feat: add short queue and refactor hpc args ([`6bc57e6`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/6bc57e6f201b479930fcd305f142318d54e37036))

## v1.8.2 (2022-09-15)

### Fix

* fix: remove redundant short option from gen_job ([`7f595eb`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/7f595eb4f5beaae45cda47648c346ad4bd263efc))

## v1.8.1 (2022-08-19)

### Fix

* fix: replace environment var CDEF for PDEF which is more reliable on CSF ([`679a1e1`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/679a1e1312881d8e4cc8a89ce00f2afb9ea3c193))

### Unknown

* Merge branch &#39;54-replace-gaussian-environment-variable-cdef-to-pdef&#39; into &#39;main&#39;

fix: replace environment var CDEF for PDEF which is more reliable on CSF

Closes #54

See merge request chilton-group/gaussian_suite!62 ([`8bab716`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/8bab7166f6a433a6b213468fe7d5e39dd48aa13d))

## v1.8.0 (2022-08-08)

### Feature

* feat: add catch for atoms with no defined basis set or ecp ([`ad19956`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/ad19956b37d300b1f7c5097ff6fcd55ef2785359))

### Unknown

* Merge branch &#39;53-add-exception-for-undefined-basis-set&#39; into &#39;main&#39;

Resolve &#34;Add exception for undefined basis set&#34;

Closes #53

See merge request chilton-group/gaussian_suite!61 ([`792d4f0`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/792d4f049cab865ee317b78e4205433c72079acc))

## v1.7.0 (2022-08-02)

### Documentation

* docs: add type hints ([`6dfc271`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/6dfc271946e41b18b9e3f30d023fab19b8bf8454))

### Feature

* feat: add charge extraction ([`1756e04`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/1756e046aca60e203ece411cc3f7f12841f0c41a))

### Unknown

* Merge branch &#39;52-add-chelpg-charge-only-extraction&#39; into &#39;main&#39;

Resolve &#34;add chelpg charge only extraction&#34;

Closes #52

See merge request chilton-group/gaussian_suite!60 ([`9f54f65`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/9f54f65b61949a0a285c341b8b50569c4ebf7425))

## v1.6.8 (2022-08-01)

### Fix

* fix: increment xyz_py release ([`1123108`](https://gitlab.com/chilton-group/gaussian_suite/-/commit/11231084531d168a853660e8d312a89aacf28687))

#! /usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np
import argparse

def read_user_input():
	#Read in command line arguments

	parser = argparse.ArgumentParser(description='Plots infrared energies from Gaussian log file(s)')
	parser.add_argument('log_files', type = int, nargs='+',  metavar = 'Gaussian log file(s)', 
						help = 'log file name(s)')
	args = parser.parse_args()

	return args

def plot_spectrum(args):

	fig, ax = plt.subplots(1,1)

	for log in args.log_files:

		data = np.loadtxt(log)

		markerline, stemlines, baseline = ax.stem(data[:,0], data[:,1], basefmt=' ', markerfmt='ob')
		#Set stem marker and line sizes
		plt.setp(markerline, 'markersize', 5, markerfacecolor='None')
		plt.setp(stemlines, 'linewidth', 0.8)



	plt.show()


if __name__ == '__main__':

	args = read_user_input()

	plot_spectrum(args)


